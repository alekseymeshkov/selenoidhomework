package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pages.MainPage;
import pages.ResultPage;

public class SelenoidTest extends BaseTest {

    private static final String BASE_URL = "https://ru.wikipedia.org";

    @Test
    public void wikiTest() {

        MainPage mainPage = new MainPage(driver);
        ResultPage resultPage = new ResultPage(driver);

        mainPage
                .open(BASE_URL)
                .searchText("Selenium")
                .openResultLink("Selenium (software)", "en");

        Assertions.assertEquals("Selenium", resultPage.getTitle());

    }

}
