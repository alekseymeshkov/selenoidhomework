package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class SearchPage extends BasePage {

    public SearchPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//a[@data-serp-pos]")
    @CacheLookup
    private List<WebElement> searchResultLinks;

    public ResultPage openResultLink(String title, String titleLang){
        for (WebElement link : searchResultLinks) {
            if (link.getAttribute("title").equals(titleLang + ":" + title)) {
                link.click();
                return new ResultPage(driver);
            }
        }
        return null;
    }
}
