package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage extends BasePage {

    public MainPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//input[@name='search']")
    @CacheLookup
    private WebElement searchField;

    @FindBy(xpath = "//div[@class='suggestions-special']/..")
    @CacheLookup
    private WebElement searchAll;

    public MainPage open(String url){
        driver.get(url);
        return this;
    }

    public SearchPage searchText(String text) {
        searchField.click();
        searchField.sendKeys(text);
        searchAll.click();
        return new SearchPage(driver);
    }

}